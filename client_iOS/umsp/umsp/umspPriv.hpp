/*
 *  umspPriv.hpp
 *  umsp
 *
 *  Created by 杨雪芹 on 16/8/15.
 *  Copyright © 2016年 geliang. All rights reserved.
 *
 */

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class umspPriv
{
	public:
		void HelloWorldPriv(const char *);
};

#pragma GCC visibility pop
