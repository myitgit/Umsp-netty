#ifndef __BASE64ER_H__
#define __BASE64ER_H__
#ifdef __cplusplus
extern "C" {
#endif
enum EBASE64 {
	BASE64_OK = 0, BASE64_INVALID
};

#define BASE64_ENCODE_OUT_SIZE(s)	(((s) + 2) / 3 * 4)
#define BASE64_DECODE_OUT_SIZE(s)	(((s)) / 4 * 3)

int base64_encode(const unsigned char *in, unsigned int inlen, char *out);

int base64_decode(char *in, unsigned int inlen, unsigned char *out);
#ifdef __cplusplus
}
#endif
#endif /* __BASE64ER_H__ */

