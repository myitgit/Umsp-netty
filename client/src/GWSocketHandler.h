/*
 * GWSocketHandler.h
 *
 *  Created on: 2016-5-4
 *      Author: Administrator
 */

#ifndef GWSOCKETHANDLER_H_
#define GWSOCKETHANDLER_H_

#include "SocketHandler.h"
#include "GWdefine.h"
/**
 * Do encode and decode
 */
class GWSocketHandler: public SocketHandler {
public:
	/**
	 * lenIndex 长度在包头中位置
	 * lenSize  长度占的字节数
	 */
	GWSocketHandler(int lenIndex,int lenSize);
	GWSocketHandler();
	virtual ~GWSocketHandler();
	//接收收据
	virtual int doDecode(SocketChannel* socketChanel, char *buf, int size);
	//发送数据
	virtual  int doEncode(int socketfd, char *buf, int size);
	virtual  int doEncode(int socketfd, char *buf, int size,int cmd,char* outBuf,int outBufSize);
	//发送数据 返回写入成功的字节数
	virtual int send(char* buf,int size);
private:
	int mLenIndex;
	int mLenSize;
};

#endif /* GWSOCKETHANDLER_H_ */
