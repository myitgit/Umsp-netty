#include "Hex.h"
unsigned char TABLE[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
		'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
		'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
void bytesToString(char* src, int srcLen, char*buf, int outLen) {
	int length = srcLen;
	if (outLen < srcLen * 2) {
		return;
	}
	for (int i = 0; i < length; i++) {
		buf[i * 2] = TABLE[(src[i] & 0xFF) >> 4]; //H
		buf[i * 2 + 1] = TABLE[((src[i] & 0x0F)) & 0xFF]; //L
	}
}

void stringToBytes(char* src, int srcLen, char* outArray, int outLen) {
	if (outLen * 2 < srcLen) {
		return;
	}
	for (int i = 0; i < outLen; i++) {
		int h = 0;
		int l = 0;
		for (int j = 0; j < 36; j++) {
			if (src[i * 2] == TABLE[j]) {
				h = j;
				break;
			}
		}
		for (int j = 0; j < 36; j++) {
			if (src[i * 2 + 1] == TABLE[j]) {
				l = j;
				break;
			}
		}
		outArray[i] = ((((h << 4 | (l & 0x0F)))) & 0xFF);
	}
}
