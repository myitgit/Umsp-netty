//============================================================================
// Name        : GWSocketClient.cpp
// Author      : Ge LIang
// Version     :
// Copyright   : @2016 GeLiang
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdlib.h>
#include <string.h>
using namespace std;

#include "common.h"
#include "GWdefine.h"
#include "Hex.h"
#include "BaseAdapter.h"

#include "SocketLooper.h"
#include "GWSocketClient.h"

int GWNativeBrige(int methodID,char* parameters, int parametersSize, char* byte,int byteSize,void *client ){


    if (parameters == NULL) {
		LOGW("null Buffer will not be send");
		return 0;
	}
    if(parametersSize>BUF_MAX_SIZE){
		LOGW("inputSize>BUF_MAX_SIZE(%d)",BUF_MAX_SIZE);
		return 0;
	}
	cJSON* jsonParams = cJSON_Parse(parameters);
	if (jsonParams == NULL) {
		LOGW("JSON Parse fail, Not a JSON String");
		return 0;
	}


    cJSON* jsonMsg = cJSON_GetObjectItem(jsonParams, "jsonMsg");
    char* stringMsg = NULL;
    if (jsonMsg != NULL)
    {
        stringMsg = jsonMsg->valuestring;
    }

    if (client == NULL) {
        LOGE("mSocketLooper is NULL");
        return 0;
    }

    int packetSize = sizeof(GWMsg);
    //ignore bytes if jsonMsg is not null .
    if(jsonMsg != NULL){
        packetSize += strlen(stringMsg);
    }else{
        packetSize += byteSize;
    }

    GWMsg* req = (GWMsg*)malloc(packetSize);
    if (req == NULL) {
        LOGE("xx OOM xx");
        return 0;
    }

    switch (methodID)
    {
        case CMD_SendToAll: {
                req->header.cmd = CMD_SendToAll;
                req->header.size = packetSize;
                memcpy(req + 1, byte, byteSize);
                LOGI("send msg [cmd:%d,size:%d]->%s", req->header.cmd, req->header.size, (char*)(req + 1));
                ((SocketLooper*)client)->putToSend((char*)(req), req->header.size);
                break; }
        case CMD_MSG_ID_FROM_TO_STRING: {
                req->header.fromID =  cJSON_GetObjectItem(jsonParams, "fromID")->valueint;
                req->header.toID = cJSON_GetObjectItem(jsonParams, "toID")->valueint;
                req->header.cmd = cJSON_GetObjectItem(jsonParams, "cmd")->valueint;
                req->header.size = packetSize;
                memcpy(req + 1, stringMsg, stringMsg==NULL?0:strlen(stringMsg));
                ((SocketLooper*)client)->putToSend((char*)(req), req->header.size);
                break; }
        case CMD_MSG_ID_FROM_TO_BYTES: {
                req->header.fromID = cJSON_GetObjectItem(jsonParams, "fromID")->valueint;
                req->header.toID = cJSON_GetObjectItem(jsonParams, "toID")->valueint;
                req->header.cmd = cJSON_GetObjectItem(jsonParams, "cmd")->valueint;
                req->header.size = packetSize;
                memcpy(req + 1, byte, byteSize);
                LOGI("send msg [cmd:%d,size:%d]->%s", req->header.cmd, req->header.size, (char*)(req + 1));
                ((SocketLooper*)client)->putToSend((char*)(req), req->header.size);
                break; }
        case CMD_MSG_JSON: {
                req->header.cmd = cJSON_GetObjectItem(jsonParams, "cmd")->valueint;
                req->header.size = packetSize;
                memcpy(req + 1, stringMsg, stringMsg==NULL?0:strlen(stringMsg));
                LOGI("send msg [cmd:%d,size:%d]->%s", req->header.cmd, req->header.size, (char*)(req + 1));
                ((SocketLooper*)client)->putToSend((char*)(req), req->header.size);
                break; }
        default:
            LOGW("send fail,Unknown CMD %d", methodID);
            return methodID;
        break;
        }
    free(req);

    return 0;
}


int initMethodBirdge() {
    setMethodBridge(GWNativeBrige);
    return 0;
}



