# Umsp-netty

#### Description
Message push(synchronize) and Instant Messaging,base on netty

分布式数据分发，群组广播，网络数据传输框架，适用于视频通话，Moba等实时联网游戏场景

#### Software Architecture
CS结构
- 服务端为Java，基于NIO框架Netty
- 客户端为C/C++、Java、C#，JavaScript、TypeScirpt。跨平台适配，支持Andoid，iOS，Win、Mac、Linux，H5平台

#### Feature
1. 性能不错, 可动态扩容, 理论上只要机器够,支持海量的玩家同时在线进行频繁的数据交互,如MOBA游戏
2. 可同时支持 私有tcp/websocket/kcp协议. 适用于H5/Native游戏二次开发
3. 玩家匹配服务独立, 匹配规则自定义,甚至可将客户端链接调度到localhost的机器上,方便调试.
4. 编译部署方便, 配合idea一键打包, 4个jar包走天下(4个服务可分开部署)

#### Installation

1. 服务端工程用Idea或者Eclipse打开。本地依赖,可离线运行
2. 客户端Android Studio、WebStrom，CLion，VS
3. JS的Demo为JavaScipt所写Html页面，用于快速开发调试新功能，及并发测试。



#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request

