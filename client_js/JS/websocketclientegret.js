try {
    if (typeof (egret) !== "undefined") {

        WebSocketClient = function WebSocketClient(host, callback) {
            var socket = null;
            var socketOpen = false;
            var socketMsgQueue = [];
            var mCallBack = callback;
            var mHost = host;
            var that = this;
            this.close = function () {
                if (socket) {
                    socket.close();
                }
            };
            /**
             * msg {DataView}
             */
            this.send = function (msg) {

                if (socketOpen) {
                    var byte = new egret.ByteArray();
                    byte.position = 0;
                    var len = msg.buffer.byteLength;
                    for (var i = 0; i < len; i++) {
                        byte.writeByte(msg.getUint8(i));
                    }
                    socket.writeBytes(byte, 0, byte.bytesAvailable);
                } else {
                    //只缓存一百
                    if (socketMsgQueue.length < 100) {
                        socketMsgQueue.unshift(msg);
                    }
                }
            };


            var onOpen = function (res) {
                console.log("[egret.WebSocket][connect]:" + res);
                socketOpen = true;
                while (socketMsgQueue.length > 0) {
                    that.send(socketMsgQueue.pop());
                }

                mCallBack.onConnect && mCallBack.onConnect(mHost);
            };

            var onClose = function (e) {
                socketOpen = false;
                mCallBack.onDisConnect && mCallBack.onDisConnect(0, "wx.onClose", mHost);
                console.log("[egret.WebSocket] [onClose] case:" + e);
            };

            var onMessage = function () {
                var byte = new egret.ByteArray();
                socket.readBytes(byte);
                var buffer = new ArrayBuffer(byte.readAvailable);
                var dataView = new DataView(buffer);
                for (var i = 0; i < buffer.byteLength; i++) {
                    dataView.setUint8(i, byte.readUnsignedByte());
                }
                mCallBack.onMsg(dataView);
            };

            var onError = function (e) {
                mCallBack.onErr && mCallBack.onErr(0, "wx.onError", mHost);
                console.log("[egret.WebSocket] [onError] case:" + e);
            };

            function connect() {
                socket = new egret.WebSocket();
                socket.type = egret.WebSocket.TYPE_BINARY;
                socket.addEventListener(egret.ProgressEvent.SOCKET_DATA, onMessage, this);
                socket.addEventListener(egret.Event.CONNECT, onOpen, this);
                socket.addEventListener(egret.Event.CLOSE, onClose, this);
                socket.addEventListener(egret.IOErrorEvent.IO_ERROR, onError, this);
                socket.connectByUrl(host);
            }

            connect();
        };
    }
} catch (e) {
    console.warn("network adapter warning:" + e.message);
}