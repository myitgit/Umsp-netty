function NetWorkCallBack() {
}

NetWorkCallBack.prototype.onMsg = function (buf) {
    console.log("[INFO] onMsg ");
};
NetWorkCallBack.prototype.onErr = function (errCode, errMsg) {
    console.log("[INFO] onErr ");
};

/**
 * auto connect when new  client
 * @param host ex:  'host:port/uri?par=xx'
 * @param callback @see NetWorkCallBack
 * @constructor
 */
function WebSocketClient(host, callback) {

    this.socket = null;
    this.mCallBack = callback;
    this.mHost = host;
    var bufQueue = [];
    /**
     * send binary
     * @param message @see DataView
     */
    this.send = function (message) {

        if (!window.WebSocket) {
            return;
        }
        if (isIE()) {
            var uint8A = new Uint8Array(message.buffer.byteLength);
            for (var i = 0; i < uint8A.length; i++) {
                uint8A[i] = (message.getUint8(i));
            }
            message = uint8A;
        }
        if (this.socket.readyState === WebSocket.OPEN) {
            //log(message);
            this.socket.send(message.buffer);
        } else {
            bufQueue.push(message);
        }
    };

    this.close = function () {
        if (this.socket) {
            if (typeof cc !== "undefined" && typeof cc.Component !== "undefined") {
                this.socket.close();
            } else {
                this.socket.close(1000, "");
            }
        }
    };


    if (!window.WebSocket) {
        window.WebSocket = window.MozWebSocket;
    }

    if (window.WebSocket) {
        this.socket = new WebSocket(host);
        this.socket.hashcode = new Date().getMilliseconds();
        console.log("try to create a socket:" + this.mHost + " socket is " + this.socket.hashcode);
        this.socket.onmessage = function (event) {

            if (typeof FileReader !== 'undefined' && (event.data instanceof Blob)) {
                var reader = new FileReader();
                reader.readAsArrayBuffer(event.data);
                //  当读取操作成功完成时调用.
                reader.onload = function (evt) {
                    if (evt.target.readyState === FileReader.DONE) {
                        var dataView = new DataView(reader.result);
                        this.mCallBack.onMsg(dataView);
                    } else {
                        this.mCallBack.onErr(MvsCode.DataParseErr, "[err]parse fail");
                    }
                }.bind(this);
            } else if (event.data instanceof ArrayBuffer) {
                var dataView = new DataView(event.data);
                this.mCallBack.onMsg(dataView);
            } else {
                console.log("[error] unknown event :" + event + " => " + JSON.stringify(event));
                this.mCallBack.onErr(MvsCode.DataParseErr, "[err]parse fail");
            }
        }.bind(this);

        this.socket.onopen = function (event) {
            console.log("Create the socket is success :" + this.mHost + " socket is " + this.socket.hashcode);
            while (bufQueue.length > 0) {
                this.send(bufQueue.pop());
            }
            this.mCallBack.onConnect && this.mCallBack.onConnect(this.mHost);

        }.bind(this);

        this.socket.onclose = function (e) {
            if (typeof cc !== "undefined" && typeof cc.Component !== "undefined") {
                e = {"code": 1000, "reason": "jsb friend close "};
            }
            this.mCallBack.onDisConnect && this.mCallBack.onDisConnect(this.mHost, e);
            console.log("socket on closed ,code:" + e + "(1000:NORMAL,1005:CLOSE_NO_STATUS,1006:RESET,1009:CLOSE_TOO_LARGE)"  );

        }.bind(this);

        this.socket.onerror = function (event) {
            console.log("socket on error ,event:" + JSON.stringify(event));
            this.mCallBack.onDisConnect && this.mCallBack.onDisConnect(this.mHost, event);
        }.bind(this);

    } else {
        console.log("Not Support the WebSocket！");
    }
}


try {
    if (typeof (wx) !== "undefined") {
        HttpClient = function HttpClient(callback) {
            this.mCallback = callback;


            function send(url, callback, isPost, params) {
                wx.request({
                    url: url,
                    data: {
                        x: "",
                        y: ""
                    },
                    header: {
                        "content-type": "application/json"
                    },
                    success: function (res) {
                        var rsp = JSON.stringify(res.data);
                        log.i("http success:" + rsp);
                        callback.onMsg(rsp);
                    },
                    fail: function (res) {
                        log.i("http fail:" + res.errMsg);
                        callback.onErr(0, res.errMsg);
                    }
                });
            }

            this.get = function (url) {
                send(url, this.mCallback, false, null);
            };
            this.post = function (url, params) {
                send(url, this.mCallback, true, params);
            };
        };
    }
    else {
        HttpClient = function HttpClient(callback) {
            this.mCallback = callback;

            function send(url, callback, isPost, params) {
                var http = new XMLHttpRequest();
                http.open(isPost ? "POST" : "GET", url, true);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                http.onreadystatechange = function () {//Call a function when the state changes.
                    if (http.readyState === 4) {
                        if (http.status === 200) {
                            callback.onMsg(http.responseText);
                            log.i("[HTTP:](" + url + ")+" + http.responseText);
                        } else {
                            callback.onErr(http.status, http.statusText);
                        }
                    }
                };
                if (isPost) {
                    http.send(params);
                } else {
                    http.send(null);
                }
            }

            this.get = function (url) {
                send(url, this.mCallback, false, null);
            };
            this.post = function (url, params) {
                send(url, this.mCallback, true, params);
            };
        };
    }


    /**
     *  http client
     * @constructor
     */
    function HttpClient() {
        /**
         * HTTP GET
         * @param url {String} ,ex:'host:port/uri?par=xx'
         */
        this.get = function (url) {
            console.log("[INFO] Not support get ");
        };
        /**
         * HTTP POST
         * @param url {String} ex:'host:port/uri'
         * @param params {String} ex:'lorem=ipsum&name=binny';
         */
        this.post = function (url, params) {
            console.log("[INFO] Not support post ");
        };
    }
} catch (e) {
    console.warn("network adapter warning:" + e.message);
}



