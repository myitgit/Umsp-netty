function FakeRandom(seed) {
    this.next = function (range) {
        range = range || 100;
        var min = 0;
        seed = (seed * 9301 + 49297) % 233280;
        var rnd = seed / 233280.0;
        return Math.floor(min + rnd * (range - min));
    }

}

var random = new FakeRandom(5);
for (var i = 0; i < 10; i++) {
    console.log(random.next(100));
}

function getNowFormatDate() {
    var date = new Date();
    var ___ = "-";
    var __ = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = "[" + date.getFullYear() + ___ + month
        + ___ + strDate + " " + date.getHours() + __
        + date.getMinutes() + __ + date.getSeconds() + "."
        + date.getMilliseconds() + "]";
    return currentDate;
}

function getFormatDate(dateString) {

    var date = new Date();
    date.setTime(Number(dateString));
    var ___ = "-";
    var __ = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = "[" + date.getFullYear() + ___ + month
        + ___ + strDate + " " + date.getHours() + __
        + date.getMinutes() + __ + date.getSeconds() + "."
        + date.getMilliseconds() + "]";
    return currentDate;
}

function printLog(msg) {
    var loc = "";
    try {
        throw new Error();
    } catch (e) {
        var line = e.stack.split(/\n/)[2];
        loc = line.slice(line.lastIndexOf("/") + 1, line.lastIndexOf(")"));
    }
    var ta = document.getElementById('responseText');
    ta.value = ta.value + '\n' + getNowFormatDate() + "[" + loc + "]" + msg;
    ta.scrollTop = ta.scrollHeight;
}

function vt(viewName) {
    var elementsByName = document.getElementById(viewName);
    return elementsByName.value;
}

function svt(viewName, value) {
    var elementsByName = document.getElementById(viewName);
    return elementsByName.value = value;
}

function toArray(argument) {
    var args = [];
    for (var i = 0; i < argument.length; i++) {
        args.push(argument[i]);
    }
    return args;
}

var log = {};

log.openLog = function () {
    console.log("---- open log ----");
    log.i = console.log.bind(console
        , "[INFO] " + getNowFormatDate() + " ");
    log.e = console.error.bind(console
        , "[ERROR] " + getNowFormatDate() + " ");
};

log.closeLog = function () {
    console.log("---- close log ----");
    log.i = function () {
    };
    log.e = function () {
    };
};

log.openLog();//default, the log is opening
