try {
    // *********native与js交互 *********
    function callNativeMethod(methodName, parStr) {
        try {
            if (typeof (egret) !== "undefined") {
                egret.ExternalInterface.call(methodName, parStr);
            } else {
                console.log("[INFO] No native implement:" + methodName);
            }
        } catch (e) {
            console.log("[ERROR] callNativeMethod exception " + e);
        }
    }

    function regNative2JSCallBack(methodName, cb) {
        try {
            if (typeof (egret) !== "undefined") {
                egret.ExternalInterface.addCallback(methodName, cb);
            } else {
                console.log("[INFO] No native callback implement:" + methodName);
            }
        } catch (e) {
            console.log("[ERROR] callNativeMethod exception " + e);
        }
    }

    // *********native与js交互**********

    // function str2Bytes(str) {
    //     var pos = 0;
    //     var len = str.length;
    //     if (len % 2 !== 0) {
    //         return null;
    //     }
    //     len /= 2;
    //     var hexA = [];
    //     for (var i = 0; i < len; i++) {
    //         var s = str.substr(pos, 2);
    //         var v = parseInt(s, 16);
    //         hexA.push(v);
    //         pos += 2;
    //     }
    //     return hexA;
    //
    // }
    //
    // function bytes2Str(arr) {
    //     var str = "";
    //     for (var i = 0; i < arr.length; i++) {
    //         var tmp = arr[i].toString(16);
    //         if (tmp.length === 1) {
    //             tmp = "0" + tmp;
    //         }
    //         str += tmp;
    //     }
    //     return str;
    // }

    function dataView2Str(dataView, offset, len) {
        var str = "";
        for (var i = offset; i < len; i++) {
            var tmp = dataView.getUint8(i).toString(16);
            if (tmp.length === 1) {
                tmp = "0" + tmp;
            }
            str += tmp;
        }
        return str;
    }

    function str2DataView(str) {
        var pos = 0;
        var len = str.length;
        if (len % 2 !== 0) {
            return null;
        }
        var hexA = new DataView(new ArrayBuffer(str.length / 2));
        for (var i = 0; i < hexA.byteLength; i ++) {
            var s = str.substr(pos, 2);
            var v = parseInt(s, 16);
            hexA.setUint8(i,v);
            pos += 2;
        }
        return hexA;
    }

    //
    // function UmspHeader() {
    //     this.size = 0;
    //     this.x = 0;
    //     this.userID = 0;
    //     this.gameID = 0;
    //     this.serviceID = 0;
    //     this.cmd = 0;
    //     this.payload = "";
    //     this.toString = function () {
    //         return "size:" + this.size
    //             +" ,roomID:" + this.x
    //             +" ,userID:" + this.userID
    //             +" ,gameID:" + this.gameID
    //             +" ,serviceID:" + this.serviceID
    //             +" ,cmd:[" + (this.cmd)+"]";
    //     };
    // }

    
   //  var s = "8f00000000000000000000000000000000000000020000007b22757365724944223a313430333436332c2267616d654944223a302c226e69636b4e616d65223a2231343033343633222c22616363657373466c6167223a302c22726f6f6d506c617965725374617465223a302c2267726f75704944223a302c226d6d72223a302c226d6174636854797065223a307d";
   //
   //  var dataView = str2DataView(s);
   //  console.log("[INFO]  "+dataView);
   //
   //  var parDataView = function (msg) {
   //      var dataView = msg;
   //      var head = new UmspHeader();
   //      head.size = dataView.getInt32(0, true);
   //      head.x = dataView.getInt32(4, true);
   //      head.userID = dataView.getInt32(8, true);
   //      head.gameID = dataView.getInt32(12, true);
   //      head.serviceID = dataView.getInt32(16, true);
   //      head.cmd = dataView.getInt32(20, true);
   //
   //      return head;
   //  };
   // var ss =   parDataView(dataView);
   // console.log("[INFO] UmspHeader: "+ss);
   // console.log("[INFO] UmspHeader: "+ss.payload);

    function useNativeWebSocket() {
        console.log("======================= ");
        console.log("[INFO] use NativeSocket ");
        console.log("======================= ");

        WebSocketClient = function WebSocketClient(host, callback) {
            var socket = null;
            var socketOpen = false;
            var socketMsgQueue = [];
            var mCallBack = callback;
            var mHost = host;
            var that = this;
            this.close = function () {
                if (socket) {
                    socket.close();
                }
            };
            this.send = function (msg) {

                if (socketOpen) {
                    socket.writeBytes(msg, 0, msg.byteLength);
                } else {
                    //只缓存一百
                    if (socketMsgQueue.length < 100) {
                        socketMsgQueue.unshift(msg);
                    }
                }
            };


            var onOpen = function (res) {
                console.log("[egret.WebSocket][connect]:" + res);
                socketOpen = true;
                while (socketMsgQueue.length > 0) {
                    that.send(socketMsgQueue.pop());
                }

                mCallBack.onConnect && mCallBack.onConnect(mHost);
            };

            var onClose = function (e) {
                socketOpen = false;
                mCallBack.onDisConnect && mCallBack.onDisConnect(0, "wx.onClose", mHost);
                console.log("[egret.WebSocket] [onClose] case:" + e);
            };

            var onMessage = function (byteString) {
                mCallBack.onMsg(str2DataView(byteString));
            };

            var onError = function (e) {
                mCallBack.onErr && mCallBack.onErr(0, "wx.onError", mHost);
                console.log("[egret.WebSocket] [onError] case:" + e);
            };

            function connect() {
                socket = new NativeWebSocket();
                socket.addEventListener(NativeWebSocket.prototype.SOCKET_DATA, onMessage, this);
                socket.addEventListener(NativeWebSocket.prototype.CONNECT, onOpen, this);
                socket.addEventListener(NativeWebSocket.prototype.CLOSE, onClose, this);
                socket.addEventListener(NativeWebSocket.prototype.IO_ERROR, onError, this);
                socket.connectByUrl(host);
            }

            connect();
        };
    }


    regNative2JSCallBack("useNativeWebSocket", function () {
        useNativeWebSocket();
    });


    var nativeSocketID = 0;

    function NativeWebSocket() {
        // this.eventListenerMap = {};
        this.socketID = ++nativeSocketID;
    }

    NativeWebSocket.prototype.IO_ERROR = "IO_ERROR";
    NativeWebSocket.prototype.CLOSE = "CLOSE";
    NativeWebSocket.prototype.CONNECT = "CONNECT";
    NativeWebSocket.prototype.SOCKET_DATA = "SOCKET_DATA";

    NativeWebSocket.prototype.addEventListener = function (key, callback, that) {
        // this.eventListenerMap[keyWithSocketID]["cb"] = callback;
        // this.eventListenerMap[keyWithSocketID]["that"] = that;
        regNative2JSCallBack(this.socketID + "#" + key, function (msg) {
            callback.call(that, msg);
        });
    };

    NativeWebSocket.prototype.connectByUrl = function (url) {
        callNativeMethod("connectByUrl", this.socketID + "#" + url);
    };
    NativeWebSocket.prototype.writeBytes = function (dataView, offset, len) {
        callNativeMethod("writeBytes", this.socketID + "#" + dataView2Str(dataView, offset, len));
    };


    callNativeMethod("onWebSocketJSLoad", "egret");

    console.log("[INFO] onWebSocketJSLoading ");
} catch (e) {
    console.warn("network adapter warning:" + e.message);
}
// useNativeWebSocket();
// var ws = new WebSocketClient("xxx.com:8897", {
//         onConnect: function () {
//         },
//
//         onDisConnect: function () {
//         },
//
//         onMsg: function () {
//         }
//     }
// );
// var data = new DataView(new ArrayBuffer(24));
// data.setUint8(0,255);
// ws.send(data);
