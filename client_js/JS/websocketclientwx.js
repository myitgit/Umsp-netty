
try {
    if (typeof (wx) !== "undefined") {
        WebSocketClient = function WebSocketClient(host, callback) {
            /**
             * WebSocket 任务，可通过 wx.connectSocket() 接口创建返回。
             * @type {socket}
             */
            var socket = null;
            var socketOpen = false;
            var socketMsgQueue = [];
            var mCallBack = callback;
            var mHost = host;
            var that = this;
            this.close = function () {
                if (socket) {
                    socket.close();
                }
            };
            /**
             * msg {DataView}
             */
            this.send = function (msg) {

                if (socketOpen) {
                    socket.send({
                        data: msg.buffer
                    });
                } else {

                    //只缓存一百
                    if (socketMsgQueue.length < 100) {
                        socketMsgQueue.unshift(msg);
                    }
                }
            };


            function connect() {
                socket = wx.connectSocket({
                    url: host,
                    header: {
                        "engine": "WeiXinGame"
                    }
                });
            }

            connect();
            socket.onOpen(function (res) {
                console.log("[wx.WebSocket][connect]:" + res);
                socketOpen = true;
                while (socketMsgQueue.length > 0) {
                    that.send(socketMsgQueue.pop());
                }

                mCallBack.onConnect && mCallBack.onConnect(mHost);
            });

            socket.onClose(function (e) {
                socketOpen = false;
                mCallBack.onDisConnect && mCallBack.onDisConnect(0, "wx.onClose", mHost);
                console.log("[wx.WebSocket] [onClose] case:" + e);
            });

            socket.onMessage(function (res) {
                var dataView = new DataView(res.data);
                mCallBack.onMsg(dataView);
            });
            socket.onError(function (e) {
                mCallBack.onErr && mCallBack.onErr(0, "wx.onError", mHost);
                console.log("[wx.WebSocket] [onError] case:" + e);
            });
        };
    }
} catch (e) {
    console.warn("network adapter warning:" + e.message);
}



