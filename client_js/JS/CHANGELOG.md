# CHANGE_LOG

## 迭代3.7

时间：2018.04.04

JSSDK版本：JSSDK_v1.4.301

#### 1、优化主动退出的时候gateway断开不调用 errorResponse 接口

```javascript
umspclient.js( (engine.mEngineState &= ENGIN_ESTATE.STATE_LOGOUTING) !== ENGIN_ESTATE.STATE_LOGOUTING){
    //如果gateway 异常断开连接了就返回错误消息
    engine.mRsp.errorResponse && engine.mRsp.errorResponse(1000,"gateway disconnect");
}
```



时间：2018.04.03

JSSDK版本：JSSDK_v1.4.300

#### 1、新增 gameServerNotify 接口：

```typescript
sendEventGroupNotify(notify){}
```

- 参数 notify 结构

```javascript
/**
 *
 * @param srcUserID {number}
 * @param cpProto {string}
 * @constructor
 */
function MsGameServerNotifyInfo(srcUserID, cpProto) {
    this.srcUserId = srcUserID;
    this.cpProto = cpProto;
}
```

- 说明：接收 gameServer 发送的消息，参数 srcUserId 会是固定的 0。



## 敏捷版本3.6 

时间：2018.03.30

JSSDK版本：JSSDK_v1.4.000

#### 内容：

1、新增 getRoomListEx 和 getRoomListExResponse接口

2、新增 getRoomDetail 和 getRoomDetailResponse接口

3、新增 joinOverNotify 接口

4、优化 joinRoomResponse 接口参数 userprofile

5、优化 kickPlayerResponse 接口，添加参数 被踢者userID

6、优化 leaveRoomNotify  cpProto返回为空问题 。

