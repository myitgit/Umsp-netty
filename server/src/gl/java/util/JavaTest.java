package gl.java.util;

import gl.java.umsp.UmspHeader;
import io.netty.buffer.ByteBufUtil;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
public class JavaTest {

    public static class A {
        public int a = 10;


        public void print() {
            System.out.println("super " + a);
        }

        protected void c() {
            System.out.println("ccc");
        }
    }

    public static class B extends A {
        int a = 100;

        public void printb() {
            System.out.println("child " + a);
        }

        @Override
        public void c() {
            super.c();
        }
    }

    public static void main(String[] args) throws JSONException {

        UmspHeader umspHeader = new UmspHeader(88);
//        umspHeader.setPayload(new byte[1]);
        System.out.println(UmspHeader.fromByte(ByteBufUtil.getBytes(umspHeader.toBytBuf())));
//
//        log.info(" Mina");
//
//        long last = System.currentTimeMillis();
//
//        for (int i = 0; i < 1000000; i++) {
//            log.info(" "+i);
//        }
//        log.info(""+(System.currentTimeMillis() - last));
    }

    static int id = 0;

    public static void addNode(JSONArray jsonArray, int parent, int id) throws JSONException {
        JSONObject value = new JSONObject();
        value.put("parent", parent);
        value.put("id", id);
        jsonArray.put(value);
        System.out.println("parend" + parent + " id:" + id);
    }

    public static long GetMD5LongHash(String inputValue) {

        byte[] s;
        try {
            s = MessageDigest.getInstance("MD5").digest(inputValue.getBytes("UTF-8"));
            long result = 0;
            for (int i = 0; i < s.length; i += 2) {
                result <<= 8;
                result |= (s[i] ^ s[i + 1]);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
