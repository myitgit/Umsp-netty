package gl.java.game;

import com.sun.istack.internal.Nullable;
import gl.java.umsp.UmspNetWorkEvent;
import gl.java.umsp.UmspNetWorkEventReceiver;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Slf4j
public class GameEngine {
    public static final int TimeMinWait = 2;
    private UmspNetWorkEventReceiver mNetWorkPacketReceiver;

    private static Logger Log = LoggerFactory.getLogger(GameEngine.class);

    private volatile boolean isStart = true;
    private List<Timer> timerList = new ArrayList<Timer>();
    private Object lock = new Object();

    private static class Holder {
        private static GameEngine engine = new GameEngine();
    }


    private int TIME_WAIT_OUT = 1 * 1000;
    private Queue<UmspNetWorkEvent> queue = new ConcurrentLinkedQueue<UmspNetWorkEvent>();

    public static GameEngine getInstance() {
        return Holder.engine;
    }

    private GameEngine() {
        new Thread() {
            @Override
            public void run() {


                Thread.currentThread().setName("EgeLoop");
                log.info("engine loop start");
                while (isStart) {
                    long current = System.currentTimeMillis();
                    try {
                        loopNetWorkEvent();
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error("loopNetWorkEvent error " + e);
                    }
                    try {
                        long wait = loopTimer();
                        if (wait > TimeMinWait) {
                            synchronized (lock) {
//                                log.info("wait:"+wait);
                                lock.wait(wait > 0 ? wait : TIME_WAIT_OUT);
//                                log.info("wakeup");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error("loopTimer error " + e);
                    }
                }
                log.info("engine loop stop");
            }
        }.start();
    }

    private void loopNetWorkEvent() {
        while (!queue.isEmpty()) {
            UmspNetWorkEvent event = queue.poll();
//            log.info("loopNetWorkEvent ");
            if (queue!=null&&this.mNetWorkPacketReceiver != null) {
                this.mNetWorkPacketReceiver.onReceiver(event);
            }
        }
    }

    public boolean addNetWorkEvent(UmspNetWorkEvent p) {
        boolean offer = this.queue.offer(p);
//        log.info("addNetWorkEvent");
        notifyLoopThread();
        return offer;
    }

    private void notifyLoopThread() {
        synchronized (lock) {
            lock.notify();
        }
    }

    public void setNetWorkEventReceiver(UmspNetWorkEventReceiver receiver) {
        this.mNetWorkPacketReceiver = receiver;
    }

    /**
     * @return 返回下次执行定时任务所需等待的最小时间
     */
    private long loopTimer() {
        long current = System.currentTimeMillis();
        long minInterval = Integer.MAX_VALUE;
        List<Timer> temp = null;
        for (int i = 0; i < timerList.size(); i++) {
            Timer timer = timerList.get(i);
            if (timer!=null&&current - timer.lastInvoke >= timer.interval) {
//                log.info("timer:" + timer.ID);
                timer.runnable.run();
                timer.lastInvoke = System.currentTimeMillis();
                if (timer.disposable) {
                    if (temp == null) {
                        temp = new ArrayList<>();
                    }
                    temp.add(timer);
                } else {
                    minInterval = Math.min(minInterval, timer.interval);
                }
            }
        }
        if (temp != null && temp.size() > 0) {
            timerList.removeAll(temp);
        }
        if (minInterval == Integer.MAX_VALUE) {
            minInterval = 0;
        }
        return minInterval;
    }

    public long setTimeout(@Nullable Runnable runnable, long millisecond) {
        //log.info("[timer] be add:" + runnable);
        Timer e = new Timer(runnable, millisecond, true);
        timerList.add(e);
        notifyLoopThread();
        return e.ID;
    }


    public long setInterval(@Nullable Runnable runnable, long millisecond) {
        //log.info("[timer] be add:" + runnable);
        Timer e = new Timer(runnable, millisecond);
        timerList.add(e);
        notifyLoopThread();
        return e.ID;
    }

    public void clearInterval(@Nullable long id) {
        for (Timer timer : timerList) {
            if (id == timer.ID) {
//                log.info("[timer] be remove:" + id);
                timerList.remove(timer);
                break;
            }
        }
    }

    public void clearTimeout(@Nullable long id) {
        for (Timer timer : timerList) {
            if (id == timer.ID) {
//                log.info("[timer] be remove:" + id);
                timerList.remove(timer);
                break;
            }
        }
    }

    public void stop() {

        notifyLoopThread();
        isStart = false;
        timerList.clear();
        log.warn("engine has stop , but the network.queue.size is : " + queue.size());
        queue.clear();
        mNetWorkPacketReceiver = null;
    }
//    public static void main(String[] args){
//        GameEngine.getInstance().addNetWorkEvent(new UmspNetWorkEvent(null,null));
//        new Thread(){
//            @Override
//            public void run() {
//                for (int i = 0; i < 100; i++) {
//                    try {
//                        Thread.sleep(300);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    GameEngine.getInstance().addNetWorkEvent(new UmspNetWorkEvent(null, null));
//                }
//            }
//        }.start();
//    }
}
