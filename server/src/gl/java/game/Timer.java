package gl.java.game;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 定时器
 */
public class Timer {
    public Runnable runnable;
    public long interval;
    public long lastInvoke;
    public long ID;
    public static AtomicInteger GID = new AtomicInteger(0);
    /**
     * false是forever,true是disposable
     */
    public boolean disposable;

    public Timer(Runnable runnable, long millisecond) {
        this.runnable = runnable;
        this.interval = millisecond;
        this.lastInvoke = System.currentTimeMillis();
        this.ID = GID.incrementAndGet();
    }

    /**
     * @param runnable    function
     * @param millisecond interval
     * @param disposable  @see Timer#disposeCount
     */
    public Timer(Runnable runnable, long millisecond, boolean disposable) {
        this(runnable, millisecond);
        this.disposable = disposable;
    }
}
