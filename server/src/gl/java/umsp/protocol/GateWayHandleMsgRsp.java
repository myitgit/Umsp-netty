package gl.java.umsp.protocol;

import gl.java.umsp.ErrCode;
import gl.java.umsp.Umsp;
import gl.java.umsp.UmspHeader;
import gl.java.umsp.router.ChannelRouter;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

public class GateWayHandleMsgRsp implements IUmspHandler {
    @Override
    public boolean handle(ChannelHandlerContext ctx, UmspHeader msg) {
        if (Umsp.CMD_MSG_RSP == msg.cmd){
            Channel connectionChannel = ChannelRouter.getInstance().getChannel(msg.userID);
            if (connectionChannel == null) {
                Umsp.returnErrMsg(Umsp.CMD_ERR, ErrCode.UserIsOffline,ErrCode.toString(ErrCode.UserIsOffline),ctx.channel());
            }else{
                Umsp.returnMsg(msg, connectionChannel);
            }
            return true;
        }
        return false;
    }
}
