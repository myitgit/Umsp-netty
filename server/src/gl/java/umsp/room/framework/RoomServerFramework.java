package gl.java.umsp.room.framework;

import gl.java.umsp.UmspHeader;
import gl.java.umsp.bean.Room;
import gl.java.umsp.bean.User;
import gl.java.umsp.room.IRoomService;
import gl.java.umsp.room.RoomService;
import gl.java.umsp.room.RoomServiceBase;

import java.lang.reflect.Constructor;

public class RoomServerFramework {
    IRoomService service = new IRoomService() {
        @Override
        public boolean onUserEnter(Room room, User user) {
            return false;
        }

        @Override
        public boolean onUserExit(Room oldRoom, User user) {
            return false;
        }

        @Override
        public boolean onUserSendMsg(UmspHeader msg, Room room) {
            return false;
        }

        @Override
        public void start(Config config) {

        }

        @Override
        public void stop() {

        }
    };
    public static final String IMPLEMENTATION_SERVICE_ROOM_CLASS_NAME = "RoomServiceApplication";

    public RoomServerFramework(RoomService roomService) {
        try {
            Class c = Class.forName(IMPLEMENTATION_SERVICE_ROOM_CLASS_NAME);
            service = (IRoomService) ((Constructor) c.getConstructor(RoomService.class)).newInstance(roomService);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public IRoomService getImp() {
        return service;
    }
}
