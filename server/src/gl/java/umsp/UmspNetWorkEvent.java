package gl.java.umsp;

import io.netty.channel.ChannelHandlerContext;

public class UmspNetWorkEvent {
    public UmspNetWorkEvent(ChannelHandlerContext channelHandlerContext, UmspHeader event) {
        this.context = channelHandlerContext;
        this.frame = event;
    }
    public UmspHeader frame;
    public ChannelHandlerContext context;

}