package gl.java.umsp;

import java.util.HashMap;
public class ErrCode {
    private static final HashMap<Integer,String> ErrHashMap = new HashMap<Integer,String>();

    public static String toString(int errCode){
        String s = ErrHashMap.get(errCode);
        if (s==null||s.length()<=0){
            s = "UNKNONW ERROR case at:"+new Throwable().getStackTrace()[1].toString();
        }
        return s;
    }
    public static int NotSupportMatchType = 405;
    static{ErrHashMap.put(NotSupportMatchType,"不支持的匹配方式");}

    public static int NotLogin = 201;
    static{ErrHashMap.put(NotLogin,"用户没登录");}

    public static int RoomNotEnough = 602;
    static{ErrHashMap.put(RoomNotEnough,"没有足够的房间");}

    public static int RoomOrUserInvalid = 603;
    static{ErrHashMap.put(RoomOrUserInvalid,"匹配失败,用户或者房间信息被修改已无效,请重试");}

    public static int RoomSerViceOffline = 604;
    static{ErrHashMap.put(RoomSerViceOffline,"房间服务不在线");}

    public static int UserMatchIsFrequency = 605;
    static{ErrHashMap.put(UserMatchIsFrequency,"匹配太频繁,请稍后重试");}

    public static int UserHasInRoom = 606;
    static{ErrHashMap.put(UserHasInRoom,"用户已经在房间中");}

    public static int UserIsOffline = 607;
    static{ErrHashMap.put(UserIsOffline,"用户已经从GateWay离线");}

    public static int MatchNoHandler = 510;
    static{ErrHashMap.put(NotLogin,"没有匹配Handler");}


}
