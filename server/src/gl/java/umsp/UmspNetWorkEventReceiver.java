package gl.java.umsp;

public interface UmspNetWorkEventReceiver {
    void onReceiver(UmspNetWorkEvent netWorkPacket);
}
