package gl.java.umsp.match;

import gl.java.umsp.bean.Match;
import gl.java.umsp.bean.Room;

/**
 * 房间分配接口
 */
public interface IMatchRoomAllocate {
    Room allocateIdleRoom(int gameID, Match match);
    boolean recycleBusyRoom(String room);

}
