package gl.java.umsp.match;

/**
 * 匹配方式
 *
 * @author geliang
 * @see #RANDOM
 * @see #SAME_NAME
 * @see #SAME_NAME_WITH_PASSWORD
 * @see #GROUP
 */
public abstract class MatchType {

    /**
     * 随机快速匹配.
     */
    public static final int RANDOM = 1<<0;

    /**
     * 指定房间名匹配,人满提示失败.
     */
    public static final int SAME_NAME = 1 << 1;

    /**
     * 指定房间名携带密码进入.
     */
    public static final int SAME_NAME_WITH_PASSWORD = 1 << 2;
    /**
     * 组队匹配
     */
    public static final int GROUP = 1 << 3;

    public static String toString(int matchType) {
        String type = "";

        if ((RANDOM & matchType) == RANDOM) {
            type += "#RANDOM";
        }
        if ((SAME_NAME & matchType) == SAME_NAME) {
            type += "#SAME_NAME";
        }
        if ((SAME_NAME_WITH_PASSWORD & matchType) == SAME_NAME_WITH_PASSWORD) {
            type += "#SAME_NAME_WITH_PASSWORD";
        }
        if ((GROUP & matchType) == GROUP) {
            type += "#GROUP";
        }
        if (type.length() <= 0) {
            type += "UNKNOW";
        }


        return type;
    }
}
