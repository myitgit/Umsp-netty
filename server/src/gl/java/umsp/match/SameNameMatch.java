package gl.java.umsp.match;

import gl.java.pattern.NotImplementException;
import gl.java.umsp.ErrCode;
import gl.java.umsp.bean.Match;
import gl.java.umsp.bean.MatchResult;
import gl.java.umsp.bean.Room;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 同直匹配
 *
 * @author geliang
 */
@Slf4j
public class SameNameMatch implements IMatch {
    WaitingRoomList roomList;

    public SameNameMatch() {
        roomList = new WaitingRoomList();
        MatchUserEventDispatcher.getInstance().add(roomList);
    }

    @Override
    public void match(Match match, IMatchedCallBack listener) {
        Room idleRoom = roomList.findRoom(match, new IRoomMatchFilter() {
            @Override
            public Room filter(List<Room> list, Match match) {
                if (match.getRoomName() == null) {
                    return null;
                }
                for (Room room : list) {
                    if ((!room.isFullUser()) && match.getRoomName().equals(room.roomName)) {
                        log.info("find room  is not enough user that  same name:" + room.roomName + " roomID" + room.roomID);
                        return room;
                    }
                }
                return null;
            }
        });
        if (idleRoom == null) {
            listener.onMatched(MatchResult.createFailMatchResult(ErrCode.RoomNotEnough,match.getWantToMatchUser().userID));
        } else {
            idleRoom.putUserIntoBeMatchList(match.getWantToMatchUser());
            listener.onMatched(MatchResult.createSuccessMatchResult(match, idleRoom));
        }

    }

    @Override
    public int getMatchType() {
        return MatchType.SAME_NAME;
    }

}
