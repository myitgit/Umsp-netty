package gl.java.umsp.match;

import gl.java.umsp.bean.Match;
import gl.java.umsp.bean.Room;
import gl.java.umsp.user.IUserRoomStateChangedListener;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 待匹配的房间列表
 */
@Slf4j
public class WaitingRoomList implements IUserRoomStateChangedListener {

    List<Room> list = new CopyOnWriteArrayList();

    /**
     * //查获缺人的房间
     *
     * @return room
     */
    public Room findRoom(Match match, IMatch.IRoomMatchFilter filter) {
        Room room = filter.filter(list, match);
        if (room != null) {
            return room;
        }
        Room room1 = createRoom(match.getWantToMatchUser().getGameID());
        room1.setRoomName(match.getRoomName());
        room1.setMaxRoomUser(match.getMaxUserCount());
        room1.setGameID(match.getWantToMatchUser().getGameID());
        room1.setExtraInfo(match.getRoomTag());
        list.add(room1);
        return room1;
    }

    /**
     * //创建一个空房间
     *
     * @param gameID gameID
     * @return room
     */
    protected Room createRoom(int gameID) {
        return new Room(gameID);
    }

    /**
     * 从待匹配房间中移除玩家所在的Room,用户发送stopMatch请求时调用此方法
     * <p> 即使人未满.该房间也不会进入新的用户<p/>
     *
     * @param userID userID
     * @return room
     */
    public Room removeRoomFromWaitingRoomList(int userID) {
        Room roomBe = null;
        for (Room room : list) {
            if (room.isInRoom(userID)) {
                roomBe = room;
                break;
            }
        }
        if (roomBe != null) {
            list.remove(roomBe);
        }
        return roomBe;
    }

    /**
     * 从房间的移除被匹配到这个房间的user,
     * .
     * <p>用户从网关掉线时时调用<p/>
     *
     * @param userID userID
     * @return room 房间所在的房间
     */
    public Room removeUser(int userID) {
        Room roomBe = null;
        for (Room room : list) {
            if (room.isBeMatchRoom(userID)) {
                room.removeUserFromMatchedList(userID);
                roomBe = room;
                break;
            }
        }
        if (roomBe != null) {
            list.remove(roomBe);
        }
        return roomBe;
    }

    /**
     * 如果房间已经没有待匹配的人,则把房间从待匹配的列表中移除
     *
     * @param room room
     * @return room 如果房间被移除,则返回被移除的room,否则返回null
     */
    public Room tryRemoveIdleRoom(Room room) {
        if (room != null && room.getBeMatchedInThisRoomUserList().isEmpty()) {
            list.remove(room);
            return room;
        }
        return null;
    }

    /**
     * removeUser 和 tryRemoveIdleRoom 两个方法的组合
     *
     * @param userID userID
     * @return Room @see #tryRemoveIdleRoom
     * @see #removeUser(int)
     * @see #tryRemoveIdleRoom(Room)
     */
    public Room removeUserAndTryRemoveIdleRoom(int userID) {
        return tryRemoveIdleRoom(removeUser(userID));
    }

    @Override
    public void onUserLeave(int userID, String roomID) {
        this.removeUser(userID);
    }

    @Override
    public void onUserEnter(int userID, String roomID) {
        //TODO onUserEnter
    }

    @Override
    public void onUserDisconnect(int userID) {
        this.removeUser(userID);
    }
}
