package gl.java.umsp.gateway;

import gl.java.umsp.IDistributeService;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 服务器配置信息,如区服务信息.
 */
@EqualsAndHashCode(callSuper = true)
@Builder
@Data
public class GateWayServiceConfig extends IDistributeService.Config{
//    public static final String KEY_GATEWAY_HOST = "KEY_GATEWAY_HOST";
//    public static final String KEY_GATEWAY_PORT = "KEY_GATEWAY_PORT";

    public  static GateWayServiceConfig getDefaultConfig(){
        return GateWayServiceConfig.builder().host("127.0.0.1").port(12345).build();
    }
    public String host;
    public int port;
    public int getServiceIndex;
    public String toString(){
        return super.toString();
    }

}
