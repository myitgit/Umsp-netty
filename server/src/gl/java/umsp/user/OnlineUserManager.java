package gl.java.umsp.user;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OnlineUserManager {
    public static final String KEY_REDIS = "OnlineUserHashMap";

    private OnlineUserManager() {
    }
}
