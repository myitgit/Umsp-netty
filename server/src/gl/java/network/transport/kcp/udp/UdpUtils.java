package gl.java.network.transport.kcp.udp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.socket.DatagramPacket;

import java.net.InetSocketAddress;


public class UdpUtils {
    public static DatagramPacket newUdpPacket(ByteBuf data, InetSocketAddress recipient,InetSocketAddress sender) {
        return new DatagramPacket(data, recipient,sender);
    }
}
