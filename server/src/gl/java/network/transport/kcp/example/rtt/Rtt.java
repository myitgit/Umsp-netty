package gl.java.network.transport.kcp.example.rtt;

import gl.java.network.transport.kcp.core.Kcp;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class Rtt {
    public static String IP = "127.0.0.1";
//        public static String IP = "118.24.53.22";
    public int index = 0;
    public long time = 0;

    public static ByteBuf encodeRttMsg(int index, long time) {
        int size = 300;
        ByteBuf buf = Unpooled.buffer(size);
        buf.writeInt(index);
        buf.writeLong(time);
        buf.writeShort(size);
        buf.writeBytes(new byte[size - 12]);
        return buf;
    }

    public static Rtt decodeRttMsg(ByteBuf buf) {
        if (buf.readableBytes() >= 300) {
            Rtt r = new Rtt();
            r.index = buf.readInt();
            r.time = buf.readLong();
            buf.readBytes(new byte[300 - 12]);
            return r;
        }
        return null;
    }
}
