package gl.java.network.transport.kcp.core;

import io.netty.buffer.ByteBuf;

/**

 */
public interface KcpOutput {

    void out(ByteBuf data, Kcp kcp);

}
