package gl.java.network.transport.kcp.core;

import io.netty.channel.Channel;

import java.net.InetSocketAddress;

/**

 */
public interface UkcpChannel extends Channel {


    @Override
    UkcpChannelConfig config();

    /**
     * 分配conv,表示会话编号，和tcp的 conv一样，conv相同的数据包才能够被通信双方认可
     * @return
     */
    int conv();

    @Override
    InetSocketAddress localAddress();

    @Override
    InetSocketAddress remoteAddress();

    UkcpChannel channelID(int ID);
    int channelID();

    void requestConv();
}
