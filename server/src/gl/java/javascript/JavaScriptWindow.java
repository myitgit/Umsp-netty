//package gl.java.javascript;
//
//
//import com.sun.istack.internal.Nullable;
//import jdk.nashorn.api.scripting.ScriptObjectMirror;
//import lombok.extern.java.Log;
//
//
//import java.util.List;
//import java.util.concurrent.CopyOnWriteArrayList;
//
//@Log
//public class JavaScriptWindow {
//
//    private volatile boolean isStart;
//    private List<Timer> timerList = new CopyOnWriteArrayList<>();
//
//    public JavaScriptWindow() {
//        new Thread(() -> {
//            Thread.currentThread().setName("JSThread");
//            while (true) {
//                try {
//                    long current = System.currentTimeMillis();
//                    loopTimer();
//                    if (System.currentTimeMillis() - current < 2) {
//                        Thread.sleep(1);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e1) {
//                        e1.printStackTrace();
//                    }
//                }
//            }
//        }).start();
//    }
//
//    private void loopTimer() {
//        long current = System.currentTimeMillis();
//        for (int i = 0; i < timerList.size(); i++) {
//            Timer timer = timerList.get(i);
//            if (current - timer.lastInvoke >= timer.interval) {
////                log.info("timer:" + timer.runnable);
//                timer.runnable.call(timer.runnable);
//                timer.lastInvoke = System.currentTimeMillis();
//                if (timer.disposable) {
//                    timerList.remove(timer);
//                }
//            }
//        }
//    }
//
//    public long setTimeout(@Nullable ScriptObjectMirror runnable, long millisecond) {
//        //log.info("[timer] be add:" + runnable);
//        Timer e = new Timer(runnable, millisecond, true);
//        timerList.add(e);
//        return e.ID;
//    }
//
//
//    public long setInterval(@Nullable ScriptObjectMirror runnable, long millisecond) {
//        //log.info("[timer] be add:" + runnable);
//        Timer e = new Timer(runnable, millisecond);
//        timerList.add(e);
//        return e.ID;
//    }
//
//    public void clearInterval(@Nullable long id) {
//        for (Timer timer : timerList) {
//            if (id == timer.ID) {
//
//                log.info("[timer] be remove:" + id);
//                timerList.remove(timer);
//                break;
//            }
//        }
//    }
//
//    public void clearTimeout(@Nullable long id) {
//        for (Timer timer : timerList) {
//            if (id == timer.ID) {
//                log.info("[timer] be remove:" + id);
//                timerList.remove(timer);
//                break;
//            }
//        }
//    }
//
//
//}
