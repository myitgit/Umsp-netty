//package gl.java.javascript;
//
//import lombok.extern.slf4j.Slf4j;
//import org.slf4j.LoggerFactory;
//
//import javax.script.Invocable;
//import javax.script.ScriptEngineManager;
//import javax.script.ScriptException;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//
//@Slf4j
//public class ScriptEngine {
//    private static javax.script.ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
//
//    private static final Object window = new JavaScriptWindow();
//
//    static {
//        try {
//            engine.put("JavaLogger", LoggerFactory.getLogger(ScriptEngine.class));
//            engine.put("window", ScriptEngine.window);
//            engine.eval("var console = {};\n" +
//                    "console.log = function(msg){JavaLogger.info(msg)};\n" +
//                    "console.log(\"[INFO] define window,console \");");
//
//            engine.eval("var setTimeout= function (func,timeoutInMS) {\n" +
//                    "    return window.setTimeout(func,timeoutInMS);\n" +
//                    "};\n" +
//                    "var setInterval= function (func,timeoutInMS) {\n" +
//                    "    return window.setInterval(func,timeoutInMS);\n" +
//                    "};\n" +
//                    "var clearInterval = function (ID) {\n" +
//                    "    window.clearInterval(ID);\n" +
//                    "};\n" +
//                    "var clearTimeout = function (ID) {\n" +
//                    "    window.clearTimeout(ID);\n" +
//                    "};");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.error(e.toString());
//        }
//    }
//
//    public static javax.script.ScriptEngine getEngine() {
//        return engine;
//    }
//    private   final static  int ERR_LOAD_SCRIPT_BY_PATH = 1;
//    private   final static  int ERR_NOTHING = 0;
//    private   final static  int ERR_LOAD_SCRIPT_BY_URL = 2;
//    private   volatile static  int lastError = ERR_NOTHING;
//    public   final static   Object NULL = new Object(){
//        public String toString(){return "";}
//    };
//    public static void loadScriptFileByPath(String path) throws FileNotFoundException, ScriptException {
//        try {
//
//            engine.eval(new FileReader(path));
//        } catch (Exception e) {
//            log.warn(e.toString());
//            lastError = ERR_LOAD_SCRIPT_BY_PATH;
//        }
//
//    }
//
//    public static void loadScriptFileByUrl(String url) throws ScriptException {
//        try {
//            engine.eval("load('" + url + "')");
//        } catch (Exception e) {
//            log.warn(e.toString());
//            lastError = ERR_LOAD_SCRIPT_BY_URL;
//        }
//
//    }
//
//    public static <T> Object invokeFunction(String name, Object... args) {
//        if(lastError!=ERR_NOTHING){
//            return NULL;
//        }
//        try {
//            return ((Invocable) engine).invokeFunction(name, args);
//        } catch (Exception e) {
//            log.warn(e.toString());
//            return NULL;
//        }
//    }
//
////    public static void main(String[] args) throws Exception {
////        engine.put("roomService", RoomService.INSTANCE);
////
////        ScriptEngine.loadScriptFileByPath("F:\\netty\\Umsp\\Umsp\\server\\script\\script.js");
////        File f = new File("test.txt");
////        //将File对象f直接注入到js脚本中并可以作为全局变量使用
////        engine.put("file", f);
////
////        // evaluate a script string. The script accesses "file"
////        // variable and calls method on it
////        engine.eval("print(file.getAbsolutePath())");
////        long t1 = System.currentTimeMillis();
////        for (int i = 0; i < 1000000; i++) {
////            Object getRoomServerConfig = ScriptEngine.invokeFunction("getRoomServerConfig");
////        }
////        long t2 = System.currentTimeMillis();
////        System.out.println(t2 - t1);
////
////        Invocable invocable = (Invocable) engine;
////
////        UmspHeader msg = new UmspHeader();
////        Room room = new Room(0);
////
////        long t3 = System.currentTimeMillis();
////        for (int i = 0; i < 1000 * 10000; i++) {
////            if (null != ScriptEngine.invokeFunction("onUserSendMsg", msg, room)) {
////            }
////        }
////        long t4 = System.currentTimeMillis();
////
////        log.info("100 0000 invoke:" + (t4 - t3));
////        Object result = invocable.invokeFunction("fun1", "Peter Parker");
////        System.out.println(result);
////        System.out.println(result.getClass());
////    }
//}
