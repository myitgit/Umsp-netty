[TOC]
# GateWay的设计

- @autor geliang
- @date 20170928 v1.0 初始设计

### 接口
     保持用户场连接,转发Match,HeartBeat,代理房间消息
    - handleHeartBeat
    - handleUserLogin
    - handleUserLogout
    - handleUserMatch
    - handleRoomLogin
    - handleUserMatchResult
    - handleUserMsg
       
    
### 流程
```text
1. start
    1.1 读取配置,自身的内网IP,Port
    1.2 发GateWayServiceStart广播,广播自己的内网IP,Port
    1.3 监听jvm ShutDown事件,调用stop
2. 监听消息
    2.1 初始化 `UmspMessageDispatcher`,加载接口实现代码,填充`HandlerList`
    2.2 监听消息并通过HandlerList,dispatch消息
    
3. stop
    3.1 设置服务不可用,发GateWayServiceStop广播,清除redis
```


### 数据结构
- User:
```json
  {"userID":0,"gameID":0,"accessFlag":0,"roomPlayerState":0,"roomID":0,"groupID":0,"mmr":0}
```

- Room:
```json
    {
      "roomID": 0,
      "roomIP": 0,
      "roomPort": 0,
      "gameID": 200661,
      "maxRoomUser": 2,
      "roomState": 0,
      "roomUserList": [],
      "delayForRemoveOfflineUser": 0
    }
```
### 安全校验算法
    String session = des("key", gameID+@+roomID+@+UserID);
    key配置在RoomServiceConfig放在Redis中

### RoomService的GateWay转发
    使得RoomService在本地也可以调试,原理:
    1. RoomService在启动时额外启动一个连接,连向GateWay,同时发送roomlogin指令
    2. GateWay在接收到用户消失时通过这个连接将消息转发给GateWay
    3. Room需定期向GateWay发送心跳
    RoomService无法获取用户数据.
    ps: 这种方式只能有限开放,存在较大安全隐患,必须采取一定授权失效机制.如token过期