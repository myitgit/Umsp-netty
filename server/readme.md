[TOC]

### GetStart
启动网关
nohup java -jar UmspGateWay.jar  >/dev/null 2>&1 &

### 架构
[!img](/doc/分布式设计/基于sub-pub 机制分布式设计和的房间分配流程图)
### 关键数据结构

### 快速开始
    [!link]/GetStart.md

### 名次解释

    房间人满: 当前房间人数达到房间人数上线
    房间空闲: 房间没有任何人
    快速匹配: 随机将玩家匹配到一个房间

### 数据结构

#### RoomServer:
```json
    {
      "GameName": "斗地主",
      "RoomServiceName": "一区",
      "GameID": 0,
      "RoomAreaIndex": 0,
      "MAX_USER_ON_SERVICE": 65536,
      "MaxRoom": 16384,
      "MaxUserPerRoom": 4,
      "RoomIDBaseIndex": 0,
      "IP": "未指定的外网IP",
      "PORT": "未指定的外网端口",
      "IsProxy": true
     }
```

#### Room:
```json
    {
      "roomID": 0,
      "roomIP": 0,
      "roomPort": 0,
      "gameID": 200661,
      "maxRoomUser": 2,
      "roomState": 0,
      "roomUserList": [],
      "delayForRemoveOfflineUser": 0
    }
```

#### User:
```json
    {
      "userID": 2,
      "gameID": 200660,
      "nickName": "geliang2",
      "roomPlayerState": 0,
      "roomID": 0,
      "groupID": 0,
      "mmr": 0
    }
```
#### RoomPlayerState:

```java
    package gl.java.umsp.bean;

    /**
     * 房间内玩家状态
     *
     * @author geliang
     */
    public interface RoomPlayerState {
        /**
         * 在房间外
         */
        public final static int IDLE = 0;
        /**
         * 在房间内并且准备就绪
         */
        public final static int READY = 1;
        /**
         * 在房间内没有准备
         */
        public final static int UNREADY = READY<<2;
        /**
         * 在房间内游戏结算完成
         */
        public final static int OVER = READY<<3;
        /**
         * 在房间内游戏中
         */
        public final static int PLAY = READY<<4;
        /**
         * 在房间内离线中(失去网络连接)
         */
        public final static int OFFLINE = READY<<5;
    }

```

#### MatchType:

```java
    package gl.java.umsp.match;

    /**
     * 匹配方式
     *
     * @author geliang
     * @see #RANDOM
     * @see #SAME_NAME
     * @see #SAME_NAME_WITH_PASSWORD
     * @see #GROUP
     */
    public abstract class MatchType {

        /**
         * 随机快速匹配.
         */
        public static final int RANDOM = 1;

        /**
         * 指定房间名匹配,人满提示失败.
         */
        public static final int SAME_NAME = 1 << 1;

        /**
         * 指定房间名携带密码进入.
         */
        public static final int SAME_NAME_WITH_PASSWORD = SAME_NAME << 2;
        /**
         * 组队匹配
         */
        public static final int GROUP = SAME_NAME << 3;

        public static String toString(int matchType) {
            String type = "";

            if ((RANDOM & matchType) == RANDOM) {
                type += "#RANDOM";
            }
            if ((SAME_NAME & matchType) == SAME_NAME) {
                type += "#SAME_NAME";
            }
            if ((SAME_NAME_WITH_PASSWORD & matchType) == SAME_NAME_WITH_PASSWORD) {
                type += "#SAME_NAME_WITH_PASSWORD";
            }
            if ((GROUP & matchType) == GROUP) {
                type += "#GROUP";
            }
            if (type.length() <= 0) {
                type += "UNKNOW";
            }


            return type;
        }
    }

```