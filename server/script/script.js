// var window = {};
// var console = {};
// console.log = print;
console.log("[INFO] load script ");
// console.log("[INFO] 1 " );
// var count = 0;
// for (var i = 0; i < 1000 * 1000; i++) {
//     count++;
//
// }
// console.log("[INFO] 2 count" + count);
// var setTimeout= function (func,timeoutInMS) {
//     window.setTimeout(func,timeoutInMS);
// };
var loop = 0;
var loop1 = setInterval(function () {
    loop++;
    console.log("[INFO]  loop1 " + loop);
}, 3000);

var loop2 = setInterval(function () {
    loop++;
    console.log("[INFO]  loop2 " + loop);
}, 1000);

console.log("[INFO] id " + loop1);
console.log("[INFO] id " + loop2);
setTimeout(function () {
    console.log("[INFO] clear 1 ");
    clearInterval(loop1);
    clearInterval(loop2);
    console.log("[INFO] clear 2");
}, 4000);
// var clearInterval = function (ID) {
//     window.clearInterval(ID);
// };
// var clearTimeout = function (ID) {
//     window.clearTimeout(ID);
// };
function getRoomServerConfig() {
    return JSON.stringify(
        {
            gameID: 0,
            IP: "127.0.0.1",
            port: 8899,
            portWebSocket: 8898,
            serverIndex: 1,
            desKey: "des-room"
        }
    );
}


//'Umsp' is a java object that it be inject to javascript scope when init javascript engine
var onUserSendMsg = function (msg, room) {
    console.log("onUserSendMsg:" + msg.payload2String() + " from userID:" + msg.userID);

    //
    // {"msgType":"input","data":0}
    var data = JSON.parse(msg.payload2String());
    switch (data.msgType) {
        case 'input':
            var s1 = {msgType: 'move', data: "me"};
            var s = {msgType: 'move', data: "other"};
            Umsp.broadcastOther(Umsp.buildStringMsg(JSON.stringify(s)), room, msg.userID);
            Umsp.broadcastMe(Umsp.buildStringMsg(JSON.stringify(s1)), room, msg.userID);
            break;
        default:
            Umsp.broadcast(msg, room);
            break;
    }
//    print("room.roomID : " + room.roomID);

};


var roomMap = {};
var onUserExit = function (user, room) {
    if (roomMap[room.roomID] != undefined && room.roomUserList.size() == 0) {
        var loop = roomMap[room.roomID].loop;
        clearInterval(loop);
        roomMap[room.roomID].loop = null;
        roomMap[room.roomID] = undefined;
        console.log("[INFO] room is delete " + room.roomID);
    }

};
var onUserEnter = function (user, room) {
    if (roomMap[room.roomID] == undefined) {
        roomMap[room.roomID] = room;
        initRoomLoop(room);
    }
    var userMap = room.roomUserList;

    for each(var e in userMap.keySet())
    {
        console.log("[INFO] e " + e + " :" + userMap.get(e));
    }

};

var initRoomLoop = function (room) {
    var x =0;
    room.loop = setInterval(function () {
        x++;
        var s1 = {msgType: 'move', data: x};
        Umsp.broadcast(Umsp.buildStringMsg(JSON.stringify(s1)), room);
        console.log("[INFO]  room loop ,roomID: " + room.roomID);
    }, 1000);
};
