/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : glmall

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2016-05-16 00:44:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) DEFAULT NULL,
  `bannerurl` varchar(1024) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `isvalue` int(1) unsigned zerofill DEFAULT NULL,
  `contexturl` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of activity
-- ----------------------------

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('1', 'JFinal Demo Title here', 'JFinal Demo Content here');
INSERT INTO `blog` VALUES ('2', 'test 1', 'test 1');
INSERT INTO `blog` VALUES ('3', 'test 2', 'test 2');
INSERT INTO `blog` VALUES ('4', 'test 3', 'test 3');
INSERT INTO `blog` VALUES ('5', 'test 5', 'test 4');
INSERT INTO `blog` VALUES ('6', 'sfs', 'sfdf');
INSERT INTO `blog` VALUES ('7', 'sssssssss', 'sssssssssssssssssssssssssss');
INSERT INTO `blog` VALUES ('8', 'sss', 'ssssssssssssssss');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `forderid` int(32) NOT NULL,
  `fuserid` int(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `isused` int(1) unsigned zerofill DEFAULT NULL,
  `limit` int(32) unsigned zerofill DEFAULT NULL,
  `value` int(32) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderuserid` (`fuserid`),
  KEY `forderid` (`forderid`),
  CONSTRAINT `coupon_ibfk_1` FOREIGN KEY (`forderid`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `coupon_ibfk_2` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of coupon
-- ----------------------------

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `fgoodstype` int(32) NOT NULL,
  `price` float(32,0) unsigned zerofill NOT NULL,
  `discount` float(32,0) unsigned zerofill DEFAULT NULL,
  `contexturl` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fgoodstype` (`fgoodstype`),
  CONSTRAINT `fgoodstype` FOREIGN KEY (`fgoodstype`) REFERENCES `goodstype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of goods
-- ----------------------------

-- ----------------------------
-- Table structure for goodstype
-- ----------------------------
DROP TABLE IF EXISTS `goodstype`;
CREATE TABLE `goodstype` (
  `id` int(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of goodstype
-- ----------------------------

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `ordertype` int(4) DEFAULT NULL,
  `fuserid` int(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `ispay` int(1) unsigned zerofill DEFAULT NULL,
  `paytype` int(4) unsigned zerofill DEFAULT NULL,
  `totalprice` int(32) unsigned zerofill DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `recvname` varchar(255) DEFAULT NULL,
  `recvphone` varchar(255) DEFAULT NULL,
  `sendername` varchar(255) DEFAULT '',
  `orderphone` varchar(255) DEFAULT '',
  `postmanname` varchar(255) DEFAULT '',
  `postmanphone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuserid` (`fuserid`),
  CONSTRAINT `fuserid` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for ordergoods
-- ----------------------------
DROP TABLE IF EXISTS `ordergoods`;
CREATE TABLE `ordergoods` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `forderid` int(32) DEFAULT NULL,
  `fgoodsid` int(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderuserid` (`forderid`),
  KEY `fgoodsid` (`fgoodsid`),
  CONSTRAINT `fgoodsid` FOREIGN KEY (`fgoodsid`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ordergoods_ibfk_1` FOREIGN KEY (`forderid`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ordergoods
-- ----------------------------

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `forderid` int(32) NOT NULL,
  `fuserid` int(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `ispay` int(1) unsigned zerofill DEFAULT NULL,
  `paytype` int(4) unsigned zerofill DEFAULT NULL,
  `totalprice` int(32) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderuserid` (`fuserid`),
  KEY `forderid` (`forderid`),
  CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`forderid`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of payment
-- ----------------------------

-- ----------------------------
-- Table structure for recommend
-- ----------------------------
DROP TABLE IF EXISTS `recommend`;
CREATE TABLE `recommend` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) DEFAULT NULL,
  `bannerimgurl` varchar(1024) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `isvalue` int(1) unsigned zerofill DEFAULT NULL,
  `contexturl` varchar(1024) DEFAULT NULL,
  `fgoodstype` int(32) DEFAULT NULL,
  `fgoodsid` int(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `goodsid` (`fgoodsid`),
  KEY `goodstype` (`fgoodstype`),
  CONSTRAINT `goodstype` FOREIGN KEY (`fgoodstype`) REFERENCES `goodstype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `goodsid` FOREIGN KEY (`fgoodsid`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recommend
-- ----------------------------

-- ----------------------------
-- Table structure for shoppingcar
-- ----------------------------
DROP TABLE IF EXISTS `shoppingcar`;
CREATE TABLE `shoppingcar` (
  `id` int(32) NOT NULL,
  `fuserid` int(32) NOT NULL,
  `fgoodsid` int(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shoppingoreruserid` (`fuserid`),
  KEY `shoppingorergoodsid` (`fgoodsid`),
  CONSTRAINT `shoppingorergoodsid` FOREIGN KEY (`fgoodsid`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shoppingoreruserid` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shoppingcar
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `paypassword` varchar(32) DEFAULT NULL,
  `nickname` varchar(32) DEFAULT NULL,
  `tel` varchar(32) DEFAULT NULL,
  `avatar` varchar(1024) DEFAULT NULL,
  `avatarhd` varchar(1024) DEFAULT NULL,
  `memberpoints` int(32) DEFAULT NULL,
  `memberpointsbalance` int(32) DEFAULT NULL,
  `banlance` int(32) DEFAULT NULL,
  `sacncode` varchar(1024) DEFAULT NULL,
  `sacncodeimage` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
