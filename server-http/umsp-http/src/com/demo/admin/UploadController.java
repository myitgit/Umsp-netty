package com.demo.admin;

import gl.java.utils.GoodsReader;
import gl.java.utils.ZipUtils;

import java.io.File;

import com.demo.controls.BaseController;
import com.jfinal.upload.UploadFile;

public class UploadController extends BaseController {
	public void index() {
		render("upload.html");
	}

	public void imgzipupload() {
		try {
			UploadFile up = getFile();
			File fileByUpload = up.getFile();
			System.out.println(fileByUpload.getAbsolutePath());
			String dstPath = fileByUpload.getParentFile().getParent();
			System.out.println(dstPath);
			ZipUtils.unzip(fileByUpload.getAbsolutePath(), dstPath);
			renderText("upload success");
		} catch (Exception e) {
			renderText("" + e.getMessage());
			e.printStackTrace();
		}
	}
	public void txtupload() {
		try {
			UploadFile up = getFile();
			File fileByUpload = up.getFile();
			System.out.println(fileByUpload.getAbsolutePath());
			GoodsReader.main(new String[]{fileByUpload.getAbsolutePath()});
			renderText("upload success");
		} catch (Exception e) {
			renderText(e.getMessage());
			e.printStackTrace();
		}
	}
}