package com.demo.common.config;

import gl.java.utils.OSUtils;

import java.io.File;

import com.demo.admin.EditGoodsController;
import com.demo.admin.MsgPushController;
import com.demo.admin.UploadController;
import com.demo.blog.BlogController;
import com.demo.common.model._MappingKit;
import com.demo.controls.ActivityController;
import com.demo.controls.CalendarController;
import com.demo.controls.CouponController;
import com.demo.controls.GoodsController;
import com.demo.controls.GoodsTypeController;
import com.demo.controls.IndexController;
import com.demo.controls.LeaveMessageController;
import com.demo.controls.MsgController;
import com.demo.controls.PayOrderController;
import com.demo.controls.PlaceController;
import com.demo.controls.RecommendController;
import com.demo.controls.ShoppingCarController;
import com.demo.controls.UserController;
import com.demo.controls.UserOrderController;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.tx.TxByActionKeys;
import com.jfinal.plugin.c3p0.C3p0Plugin;

/**
 * API引导式配置
 */
public class DemoConfig extends JFinalConfig {
	private Log log = Log.getLog(getClass());
	private static String FileSavePath;

	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用PropKit.get(...)获取值
		PropKit.use("a_little_config.txt");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		initFileSystem();
	}


	/**
	 * 从配置文件中读取upload目录 分win/linux
	 * 
	 * @return
	 */
	public static String getFileSavePath() {
		if (FileSavePath == null || FileSavePath.length() <= 0) {
			if (OSUtils.isWindowsOS()) {
				FileSavePath = PropKit.get("DynamicResourceDirWindowPath",
						"c:/upload/img");
			} else {
				FileSavePath = PropKit.get("DynamicResourceDirLinux",
						"/upload/img");
			}
		}
		return FileSavePath;
	}

	/**
	 * 初始化文件系统相关.如上传文件目录
	 */
	public void initFileSystem() {
		File upload = new File(getFileSavePath());
		if (!upload.exists() || !upload.isDirectory()) {
			if (!upload.mkdirs()) {
				log.error("mkdir" + upload.getAbsolutePath() + "fail");
			}
		}
	}

	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/", IndexController.class, "/index"); // 第三个参数为该Controller的视图存放路径
		me.add("/blog", BlogController.class); // 第三个参数省略时默认与第一个参数值相同，在此即为
												// "/blog"
		me.add("/recommend", RecommendController.class); // 第三个参数省略时默认与第一个参数值相同，在此即为
															// "/blog"
		me.add("/goods", GoodsController.class);
		me.add("/goodstype", GoodsTypeController.class);
		me.add("/shoppingcar", ShoppingCarController.class);
		me.add("/userorder", UserOrderController.class);
		me.add("/payorder", PayOrderController.class);
		me.add("/leavemessage", LeaveMessageController.class);
		me.add("/user", UserController.class);
		me.add("/activity", ActivityController.class);
		me.add("/coupon", CouponController.class);
		me.add("/place", PlaceController.class);
		me.add("/msg", MsgController.class);
		me.add("/calendar", CalendarController.class);

		// admin
		me.add("/admin/goods", EditGoodsController.class);
		me.add("/admin/img", UploadController.class);
		me.add("/admin/msgpush", MsgPushController.class);
	}

	public static C3p0Plugin createC3p0Plugin() {
		return new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"),
				PropKit.get("password").trim());
	}

	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		C3p0Plugin C3p0Plugin = createC3p0Plugin();
		me.add(C3p0Plugin);

		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(C3p0Plugin);
		me.add(arp);

		// 所有配置在 MappingKit 中搞定
		_MappingKit.mapping(arp);
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.add(new TxByActionKeys("/user/regit", "/user/login"));
	}

	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {

	}

	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目 运行此 main
	 * 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		int port = 80;
		if (args != null && args.length >= 1) {
			port = Integer.parseInt(args[0]);
		}
		JFinal.start("WebRoot", port, "/", 5);
	}
}
