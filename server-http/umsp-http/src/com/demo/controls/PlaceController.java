package com.demo.controls;

import java.util.List;

import com.demo.common.model.Place;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;

public class PlaceController extends BaseController {
	public void index() {
		int userid;
		if ((userid = checkToken()) < 0) {
			return;
		}
		int pageindex = getParaToInt("pageindex", 1);
		int pageSize = getParaToInt("pagesize", 10);
		Page<Place> paginate = Place.dao.paginate(pageindex, pageSize,
				"select *", " from place where fuserid = ? order by id desc",
				userid);
		renderJson(paginate.getList());
		
	}

	public void setdefault() {
		int userid;
		if ((userid = checkToken()) < 0) {
			return;
		}
		int placeid = getParaToInt("id", 0);
		if (placeid == 0) {
			renderErr(ERR_ILLPAR,"id is 0 ");
			return;
		}
		Place bean2 = Place.dao.findById(placeid);
		if (bean2 == null || bean2.getFuserid() != userid) {
			renderErr("this place not belong " + userid);
			placeid = 0;
			return;
		}
		List<Place> list= Place.dao.find("select * from place where fuserid = ? and isdefault = 1 order by id desc",userid);
		for (int i = 0; i < list.size(); i++) {
			list.get(i).setIsdefault(FALSE);
			if(!list.get(i).update()){
				renderErr(ERR_DB, "setdefault FALSE fail "+list.get(i).getId());
				Log.getLog(getClass()).error(ERR_DB+"setdefault fail"+list.get(i).getId());
				return;
			}
		}
		bean2.setIsdefault(TRUE);
		if(!bean2.update()){
			renderErr(ERR_DB, "setdefault 1 fail"+bean2.getId());
			Log.getLog(getClass()).error(ERR_DB+"setdefault fail"+bean2.getId());
			return;
		}
		renderOK();
	}
	public void add() {
		int userid;
		if ((userid = checkToken()) < 0) {
			return;
		}
		Place bean = new Place();
		int placeid = getParaToInt("id", 0);
		if (placeid != 0) {
			Place bean2 = Place.dao.findById(placeid);
			if (bean2 == null || bean2.getFuserid() != userid) {
				renderErr("this place not belong " + userid);
				placeid = 0;
				return;
			} else {
				bean = bean2;
			}
		}
		bean.setAreaid(getParaToLong("areaid", 0l));
		bean.setCityid(getParaToLong("cityid", 0l));
		bean.setFuserid(userid);
		bean.setLatitude(getPara("latitude", "0"));
		bean.setLongitude(getPara("longitude", "0"));
		bean.setIsdefault(0);
		String place = getPara("place", "");
		if (isEmpty(place)) {
			renderErr("place is null");
			return;
		}
		bean.setPlace(place);
		String placedetail = getPara("placedetail", "");
		if (isEmpty(placedetail)) {
			renderErr("placedetail is null");
			return;
		}
		bean.setPlacedetail(placedetail);
		
		String recvusername = getPara("recvusername", "");
		if (isEmpty(recvusername)) {
			renderErr("recvusername is null");
			return;
		}
		bean.setRecvusername(recvusername);
		
		String recvusertel = getPara("recvusertel", "");
		if (isEmpty(recvusertel)) {
			renderErr("recvusertel is null");
			return;
		}
		bean.setRecvusertel(recvusertel);
		
		bean.setProvinceid(getParaToLong("provinceid", 0l));
		if (placeid == 0) {
			bean.save();
		} else {
			bean.update();
		}
		renderOK();
	}

	public void delete() {
		int userid;
		if ((userid = checkToken()) < 0) {
			return;
		}
		int placeid = getParaToInt("id", 0);
		if (placeid != 0) {
			Place bean = Place.dao.findById(placeid);
			if (bean == null || bean.getFuserid().intValue() != userid) {
				renderErr("this place not belong " + userid);
				return;
			} else {
				bean.delete();
			}
		}
		renderOK();
	}
}