package com.demo.controls;

import java.util.Enumeration;
import java.util.HashMap;

import gl.java.utils.StringUtils;

import com.demo.common.model.Goods;
import com.demo.session.TokenManager;
import com.jfinal.core.Controller;

public class BaseController extends Controller {
	public final static int TRUE = 1;
	public final static int FALSE = 0;
	public final static int DOING = 2;
	
	public final static int PAYTYPE_ALI_MOBLIE = 0;
	public final static int PAYTYPE_BLANCE = 1;
	public final static int PAYTYPE_WEIXIN_MOBLIE = 2;
	
	public final static int LOGINTYPE_DEFAULT = 0;
	public final static int LOGINTYPE_QQ = 1;
	public final static int LOGINTYPE_WEIXIN = 2;
	
	public final static int MSGTYPE_SYS = 0;
	public final static int MSGTYPE_ORDER = 1;
	public final static int MSGTYPE_LEAVEMSG = 2;
	public final static int MSGTYPE_PUSH = 3;
	public final static int MSGTYPE_FRIEND = 4;
	
	public final static int ORDERTYPE_DEFAULT = 0;
	/**
	 * 每日订单
	 */
	public final static int ORDERTYPE_DAILY = 1;
	
	/**
	 * 支付状态
	 */
	public final static int PAY_STAUS_NOT = FALSE;
	public final static int PAY_STAUS_DONE = TRUE;
	public final static int PAY_STAUS_DOING = DOING;
	
	
	
	/**
	 * 系统用户的ID
	 */
	public final static int USERID_SYSTEM = 0;
	/**
	 * 数据库异常
	 */
	public final static int ERR_DB = 600;
	/**
	 * 参数非法
	 */
	public final static int ERR_ILLPAR = 601;

	protected void renderErr(String err) {
		renderJson("err", err);
	}
	protected void renderErr(int code,String err) {
		renderJson("{\"err\":\""+err+"\",\"code\":"+code+"}");
	}

	public static boolean isEmpty(Object s) {
		return StringUtils.isEmpty(s);
	}

	void renderOK() {
		renderJson("err", "");
	}

	public int[] getParToIntArray(String paraArray) {
		if (paraArray == null) {
			return null;
		}
		if (!paraArray.contains("-")) {
			try {
				return new int[]{Integer.parseInt(paraArray)};
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		String[] split = paraArray.split("-");
		int[] temp = new int[split.length];
		for (int i = 0; i < temp.length; i++) {
			temp[i] = Integer.parseInt(split[i]);
		}
		return temp;
	}
	public long[] getParToLongArray(String paraArray) {
		if (paraArray == null) {
			return null;
		}
		if (!paraArray.contains("-")) {
			try {
				return new long[]{Long.parseLong(paraArray)};
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		String[] split = paraArray.split("-");
		long[] temp = new long[split.length];
		for (int i = 0; i < temp.length; i++) {
			temp[i] = Long.parseLong(split[i]);
		}
		return temp;
	}

	public int checkToken() {
		String para = getPara("token");
		if (para == null) {
			renderErr("token==null");
			return -1;
		}
		int userid = TokenManager.getInstance().getIDByToken(para);
		if (userid < 0) {
			renderErr("用户未登录 ");
			return -1;
		}
		return userid;
	}

	public String checkGoodids() {
		String paraArray = getPara("goodsid");
		if (paraArray == null) {
			renderErr("goodsid==null");
			return null;
		}
		return paraArray;
	}
	public String getDefaultAvtor() {
		return "/img/defaultavatar.jpg";
	}
	public Goods getGoods(int id) {
		return Goods.dao.findById(id);
	}
	
	public HashMap<String, String> getParMap(Controller payOrderController) {
		Enumeration<String> parmap =  payOrderController.getParaNames();
		HashMap<String, String> parsMap = new HashMap<String, String>();
		while (parmap.hasMoreElements()) {
			String string = (String) parmap.nextElement();
			parsMap.put(string, getPara(string));
		}
		return parsMap;
	}
}
