// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: stream.proto

package stream;

public final class Stream {
  private Stream() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\014stream.proto\022\006stream\032\014simple.proto2F\n\010" +
      "CSStream\022:\n\006Stream\022\025.stream.Package.Fram" +
      "e\032\025.stream.Package.Frame(\0010\0012N\n\rSimpleSe" +
      "rvice\022=\n\rSimpleRequest\022\025.stream.Package." +
      "Frame\032\025.stream.Package.Frameb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          stream.Simple.getDescriptor(),
        }, assigner);
    stream.Simple.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
